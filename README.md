#Arduino Dual Light Switch

*Allows control of two floor lamps indepently with a single
light switch without modifying house A/C wiring.*

An Arduino Nano and two solid state relays are house inside an outlet 
strip, which is plugged into an 'always-on' wall outlet. A repurposed 
phone charger is plugged into the switch-controlled wall outlet with
its USB cable connected to one of the Arduino's input pins.

The Arduino counts the number of flips of the light switch with a 
3/4-second timeout between flips. Given the number of flips and the 
initial state of the switch, a certain set of lamps are turned on or 
off via the solid state relays.

| Initial State        | Sequence           | Function  |
| ------------- |-------------| -----|
| DOWN      | UP | Both lamps on |
| DOWN      | UP-DOWN-UP      |   Lamp A on |
| DOWN | UP-DOWN-UP-DOWN-UP      |    Lamp B on |
| UP      | DOWN | Both lamps off |
| UP      | DOWN-UP      |   Toggles lamps, or Lamp A on if both are on |
| UP | DOWN-UP-DOWN      |    Delays 60 seconds, then both lamps off   |
| UP | DOWN-UP-DOWN-UP      |    Both lamps on  |