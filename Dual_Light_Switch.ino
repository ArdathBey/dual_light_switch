/*
NEW IN V1.4

1. Changed control and lamp pins to 8, 10, and 12 for convenient circuit building.

FUTURE NOTES

1. Test to see if the 'flip verification' if statement in the flip monitor is redundant.
2. Make sure any command ending with the switch down triggers a "both off"
   (currently just 'UP DOWN'is set that way).
*/

/*
Set binary lamps states in the form of 0bAB, where A is the state of lamp A (1 on, 0 off),
and B is the state of lamp B. Ex 0b10 means "lamp A on, lamp B off"
*/
const int a = 2;      // 0b10
const int b = 1;      // 0b01
const int off = 0;    // 0b00
const int both = 3;   // 0b11

const int lampA = 10;          // Control pin for Lamp A relay (OUTPUT)
const int lampB = 12;          // Control pin for Lamp B relay (OUTPUT)
const int light_switch = 8;    // Pin to read light switch state (INPUT)
int switch_state;              // Stores current position of the light switch
int count;                     // Counts the number of flips before the timeout
int initial;                   // Position of the switch before any flips
unsigned int delay_timer;      // The time at which the "delay off" command is triggered
bool delay_set = false;
unsigned int count_timer = 0;
int bitwise_state;          // Stores the current lamp configuration

// Sets lamps to one of the four states defined above
void set_lamps(int x) { // Accepts defined variables 'a', 'b', 'off', and 'both'
    Serial.print("lamps ");
    Serial.println(x);
    if (x >= 0 && x <= 3) {          // Only allow input of 0b00, 0b01, 0b10, 0b11
    digitalWrite(lampA, (x & 2)/2);  // Write value of left-bit to lamp A
    digitalWrite(lampB, x & 1);      // Write value of right-bit to lamp B
    
    bitwise_state = x;               // Store current lamp state
    }
}

// Toggle between lamps, do nothing if none or both lamps are on
void lamp_toggle() {
    Serial.println("Lamp toggle");
    if (bitwise_state == a) {
        set_lamps(b);
    }
    else if (bitwise_state == b) {
        set_lamps(a);
    }
}

// Arduino Setup Code
void setup() {

Serial.begin(9600); 
pinMode(lampA, OUTPUT);        // Set lampA control pin as Output
pinMode(lampB, OUTPUT);        // Set lampB control pin as Output
pinMode(light_switch, INPUT);  // Set control pin as Input

switch_state = digitalRead(light_switch);  //Store initial light switch position

}

// Arduino Loop Code
void loop() {
unsigned int time = millis();  
// Begin switch test code (green LED) for diagnostic purposes only  
  if (switch_state == HIGH) {    // Turn green LED on when pushbutton is pressed
    digitalWrite(7,HIGH);
  }
  else {                         // Turn green LED off when pushbutton is not pressed
    digitalWrite(7,LOW);
  }
// End switch test code

// Monitor for light switch flips
if (switch_state != digitalRead(light_switch)) {
  delay(50);                                          // Delay for de-bounce
  if (switch_state = digitalRead(light_switch)); {    // Verify flipped switch (can this be removed?)
    switch_state = digitalRead(light_switch);         // Store new switch position
    delay_set = false;                                // Cancels delayed-off if a new command is entered.
    count += 1;                                       // Start counting switch flips
    count_timer = time + 750;                         // Set 750ms timout for flip counting
    Serial.println("count start");
    }
  }

/*
New in 1.2: Turn both lamps on with the initial flip. Without this, the program would wait 750ms for
the flip counter to time out before turning the lamps on. Since "both on" is the most used configuration,
this code "assumes" a "both on" command is coming until the flip counter actually finishes.
In a future version, it may be desirable to "assume" the last know lamp configuration instead of "both on." 
*/
if (count_timer > time) {  // Enter while the flip counter is running
  Serial.println(count_timer - time);
  if (switch_state == HIGH && count ==1) {
    lamps(both);
  }
}

// Begin flip count printing (diagnostic only)
else if ((count > 0) && (count_timer < time)) {
  Serial.print(count);
  Serial.print(" presses, starting with ");
  if (count % 2 == 0) {
    Serial.print(digitalRead(light_switch));
    }
  if (count % 2 == 1) {
    Serial.print(!digitalRead(light_switch));
    }
  Serial.println(".");
// End flip count printing


/*
BEGIN FLIP COMMAND DEFINITIONS

Init OFF: These are the possible commands that can be sent if the light switch is initially 'down'
*/
if ( ((switch_state == HIGH) && (count % 2 == 1)) || ( (switch_state == LOW) && (count % 2 == 0) ) ) {

  switch(count) {
    case 1:                  // 'UP': Both lamps on
      set_lamps(both);
      break;
    case 2:                  // 'UP DOWN': NEW in 1.2, this prevents a short UP DOWN (<750ms) from keeping the lights on
      set_lamps(off);
      break;
    case 3:                  // 'UP DOWN UP': Lamp A on only
      set_lamps(a);
      break;
    case 5:                  // 'UP DOWN UP DOWN UP': Lamp B on only
      set_lamps(b);
      break;
  } // End switch
  
} // End Init OFF

// Init ON: These are the possible commands that can be sent if the light switch is initially 'up'
if ( ((switch_state == LOW) && (count % 2 == 1)) || ( (switch_state == HIGH) && (count % 2 == 0) ) ) {

  switch(count) {
    case 1:                           // 'DOWN': Both lamps off
      set_lamps(off);
      break;
    case 2:                           // 'DOWN UP': If both lamps on, switch to lamp A. Otherwise, toggle lamps.
      if (bitwise_state == 3) {
      set_lamps(a);
      }
      else {
      lamp_toggle();
      }
      break;
    case 3: {                           // 'DOWN UP DOWN': Delayed of by 60 sec, flashes lamp(s) to confirm
      int prev_state = bitwise_state;   // This delay is cancelled by any additional flips
      set_lamps(off);
      delay(250);
      set_lamps(prev_state);
      delay_timer = time + 5000; // Currently set to 5 seconds for testing
      delay_set = true;
      break;
      }
    case 4:                           // 'DOWN UP DOWN UP': Both lamps on 
      set_lamps(both);
      break;
  } // End switch

} // End Init ON
  count = 0;

}

// Perform delay off when delay timer runs out
if ( (delay_timer < time) && delay_set ) {
    set_lamps(off);
    Serial.println("Delay Off Completed.");
    delay_set = false;
}

 
} 
